class Stack:
    def __init__(self):
        items = []
        self.items = items

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()


starting_list = [1, 2, 3, 4, 5]
number_stack = Stack()

for num in starting_list:
    number_stack.push(num)

reversed_list = []
for i in range(len(number_stack.items)):
    reversed_list.append(number_stack.pop())

print(reversed_list)
