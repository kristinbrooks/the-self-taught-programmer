import csv

cruise_movies = ['Top Gun', 'Risky Business', 'Minority Report']
dicaprio_movies = ['Titanic', 'The Revenant', 'Inception']
washington_movies = ['Training Day', 'Man on Fire', 'Flight']
all_movies = [cruise_movies, dicaprio_movies, washington_movies]

with open('movies.csv', 'w') as movies:
    write = csv.writer(movies, delimiter=',')
    write.writerow(cruise_movies)
    write.writerow(dicaprio_movies)
    write.writerow(washington_movies)
