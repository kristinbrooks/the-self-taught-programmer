class Shape:

    def what_am_i_called(self):
        print('I am a shape')


class Rectangle(Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def calculate_perimeter(self):
        return 2 * (self.width + self.height)


class Square(Shape):
    def __init__(self, s1):
        self.s1 = s1

    def calculate_perimeter(self):
        return 4 * self.s1

    def change_size(self, length_change):
        self.s1 += length_change
        return self.s1


rectangle = Rectangle(2, 5)
rectangle.what_am_i_called()

square = Square(6)
square.what_am_i_called()
