ints_list = [1, 2, 8, 3, 4, 5, 6, 8, 4, 5, 2, 1, 3, 7, 7]

ints_dict = {}

for number in ints_list:
    if number in ints_dict:
        ints_dict[number] += 1
    else:
        ints_dict[number] = 1

for key in ints_dict:
    if ints_dict[key] == 1:
        non_dup = key   # could end up empty. better to put `print(key)` here instead

print(non_dup)

# second loop written the way the instructor wrote his:
#
# for key, value in ints_dict.items:
#   if value == 1:
#       print(key)
