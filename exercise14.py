"""
The function need a number in string format, i.e. "100"
:param string
:return float
"""


def convert(string):
    return float(string)
