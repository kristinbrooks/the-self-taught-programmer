class Rectangle:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def calculate_perimeter(self):
        return 2 * (self.width + self.height)


class Square:
    def __init__(self, s1):
        self.s1 = s1

    def calculate_perimeter(self):
        return 4 * self.s1


rectangle = Rectangle(2, 5)
print(rectangle.calculate_perimeter())

square = Square(6)
print(square.calculate_perimeter())
