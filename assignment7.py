# Bash Commands
#
# 1. Print self-taught in Bash.
#   $ echo `self-taught`
#
# 2. Navigate to your home directory from another directory using an absolute and relative path.
#    starting in /Users/ktown/workspace/t-s-t-p
#
#   ABSOLUTE:
#
#   kristin: t-s-t-p $ cd /Users/kristin
#
#   kristin: ~ $
#
#   _______________________________________________________
#
#   RELATIVE:
#
#   kristin: t-s-t-p $ ../..
