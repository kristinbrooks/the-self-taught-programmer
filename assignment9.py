# installing a package on the command line with PIP
#
# ktown: the-self-taught-programmer $ pip install times2
# Collecting times2
#   Downloading times2-0.8-py2.py3-none-any.whl (5.0 kB)
# Requirement already satisfied: pytz in /Users/ktown/opt/anaconda3/lib/python3.7/site-packages (from times2) (2019.3)
# Requirement already satisfied: python-dateutil in /Users/ktown/opt/anaconda3/lib/python3.7/site-packages (from times2) (2.8.1)
# Requirement already satisfied: six>=1.5 in /Users/ktown/opt/anaconda3/lib/python3.7/site-packages (from python-dateutil->times2) (1.14.0)
# Installing collected packages: times2
# Successfully installed times2-0.8
# ktown: the-self-taught-programmer $

