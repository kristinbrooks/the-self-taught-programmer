numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
while True:
    number_or_quit = input('Guess a number on the list (or hit q to quit): ')
    if number_or_quit == 'q':
        break
    number = float(number_or_quit)
    if number in numbers:
        print('Your number is on the list!')
    else:
        print('Your number is not on the list. :(')
