class Stack:
    def __init__(self):
        items = []
        self.items = items

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()


starting_string = 'yesterday'
string_stack = Stack()

for char in starting_string:
    string_stack.push(char)

reversed_string = ''
for i in range(len(string_stack.items)):
    reversed_string += string_stack.pop()

print(reversed_string)
