class Square:
    square_list = []

    def __init__(self, s1):
        self.s1 = s1
        self.square_list.append(self)

    def calculate_perimeter(self):
        return 4 * self.s1

    def __repr__(self):
        return '{} by {} by {} by {}'.format(self.s1, self.s1, self.s1, self.s1)


print(Square(29))
