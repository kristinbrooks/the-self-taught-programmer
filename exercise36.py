class Square:
    def __init__(self, s1):
        self.s1 = s1

    def calculate_perimeter(self):
        return 4 * self.s1

    def change_size(self, length_change):
        self.s1 += length_change
        return self.s1
