"""
This program asks the user to type a number, converts it to an integer, and prints it. If it can't convert their
input to an integer, it prints a message that says "Please type an integer."
"""
a = input('Type a number: ')
try:
    a = int(a)
    print(a)
except ValueError:
    print('Please type an integer.')
