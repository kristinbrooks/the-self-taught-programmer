class Square:
    square_list = []

    def __init__(self):
        self.square_list.append(self)


def same(obj1, obj2):
    return obj1 is obj2


square1 = Square()
square2 = Square()
print(same(square1, square2))
print(same(square1, square1))
