# given hangman code, told to edit so a random word is selected. notes tell which lines i changed
import random


def hangman():  # removed the parameter
    word_options = ['cat', 'dog', 'snake', 'hamster', 'bird', 'tarantula', 'rat']   # added line
    word_index = random.randint(0, len(word_options) - 1)   # added line
    word = word_options[word_index]     # added line
    wrong = 0
    stages = ["",
              "________        ",
              "|               ",
              "|        |      ",
              "|        0      ",
              "|       /|\     ",
              "|       / \     ",
              "|               "
              ]
    rletters = list(word)
    board = ["__"] * len(word)
    win = False
    print("Welcome to Hangman")
    while wrong < len(stages) - 1:
        print("\n")
        msg = "Guess a letter"
        char = input(msg)
        if char in rletters:
            cind = rletters.index(char)
            board[cind] = char
            rletters[cind] = '$'
        else:
            wrong += 1
        print((" ".join(board)))
        e = wrong + 1
        print("\n".join(stages[0: e]))
        if "__" not in board:
            print("You win!")
            print(" ".join(board))
            win = True
            break
    if not win:
        print("\n"
              .join(stages[0: wrong]))
        print("You lose! It was {}.".format(word))


# i initially put the new code outside the function right here above the function call, and left the parameter
# and argument as they were originally...but this is how the instructor's solution did it
hangman()   # removed argument
